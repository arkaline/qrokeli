package org.qrokeli.main.obsqr;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Spannable;
import android.widget.Toast;

import org.qrokeli.main.R;

import java.util.HashMap;
import java.util.Map;

public abstract class QrContent {
    public final String rawContent;

    public final String title;
    public final String action;
    public final Spannable content;

	protected final Context context;

    private QrContent(Context c, String s, String title, String action, Spannable content) {
		this.context = c;
        this.rawContent = s;
        this.action = action;
        this.title = title;
        this.content = content;
    }

	public Intent getActionIntent() {
		return new Intent(Intent.ACTION_VIEW, Uri.parse(rawContent));
	}

	// Helper utils: copy text to primary clipboard, split qr string into tokens etc
    private static void copyToClipboard(Context context, String s) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setPrimaryClip(ClipData.newPlainText(ClipDescription.MIMETYPE_TEXT_PLAIN, s));
		Toast.makeText(context, context.getString(R.string.text_qr_action_name),
				Toast.LENGTH_LONG).show();
    }

	public void performAction(Activity activ) {
		Intent returnIntent = new Intent();
		returnIntent.putExtra("result", rawContent);
		activ.setResult(activ.RESULT_OK,returnIntent);
		activ.finish();
		// copyToClipboard(context, rawContent);
	}

	private static Map<String, String> parse(String s, String... keys) {
		Map<String, String> tokens = new HashMap<>();

		int len = s.length();
		StringBuilder builder = new StringBuilder();
		boolean escaped = false;

		// treat a char sequence between two non-escaped semicolons
		// as a single token
		for (int i = 0; i < len; i++) {
			if (escaped) {
				builder.append(s.charAt(i));
				escaped = false;
			} else {
				if (s.charAt(i) == ';') {
					String token = builder.toString();
					for (String key : keys) {
						if (token.startsWith(key+":")) {
							tokens.put(key, token.substring(key.length()+1));
						}
					}
					builder = new StringBuilder();
				} else if (s.charAt(i) == '\\') {
					escaped = true;
				} else {
					builder.append(s.charAt(i));
				}
			}
		}
		return tokens;
	}

	public static Spannable spannable(String s) {
		return Spannable.Factory.getInstance().newSpannable(s);
	}

	public static QrContent from(Context c, String s) {
		String m = s.toLowerCase();
		if (m.matches(WebUrlContent.MATCH)) {
			return new WebUrlContent(c, s);
		} else {
			return new QrMixedContent(c, s);
		}
	}

	/** Mixed content: plain text that may contain some URLs, emails etc */
	static class QrMixedContent extends QrContent {
		public QrMixedContent(Context c, String s) {
			super(c, s, c.getString(R.string.title_text), c.getString(R.string.action_text), QrContent.spannable(s));
		}
	}

	/** Web URL */
	static class WebUrlContent extends QrContent {
		public final static String MATCH = android.util.Patterns.WEB_URL.pattern();
		public WebUrlContent(Context c, String s) {
			super(c, s, c.getString(R.string.title_url), c.getString(R.string.action_url), url(s));
		}
		private static Spannable url(String s) {
			if (!s.startsWith("http:") && !s.startsWith("https:") && !s.startsWith("ftp:")) {
				s = "http://" + s;
			}
			return QrContent.spannable(s);
		}
	}
}
