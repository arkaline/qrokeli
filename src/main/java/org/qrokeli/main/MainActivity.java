////////////////////////////////////////////////////////////////////////////////
//
//  QRokèli - Aide à l'apprentissage des langues
//
//  Copyright © 2021  Marie-Pierre Brungard
//
////////////////////////////////////////////////////////////////////////////////

package org.qrokeli.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.text.InputType;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
public class MainActivity extends Activity implements TextToSpeech.OnInitListener {
    public final static String PATH = "path";
    public final static String CONTENT = "content";
    public final static String TEXTVIEW = "textview";

    public final static String PREF_PATHS = "pref_paths";
    public final static String PREF_SIZE = "pref_size";
    public final static String PREF_SPEECH_RATE = "pref_speech_rate";
    public final static String PREF_MEMTEXT = "pref_memtext";
    public final static String PREF_LANGUAGE = "pref_language";
    public final static String PREF_MODE = "pref_mode";
    public final static String PREF_FONT = "font/accessibledfa.ttf";

    public final static String DOCUMENTS = "Documents";
    public final static String FOLDER = "Folder:  ";

    public final static String DEFAULT_FILE = "qrokeli";

    public final static int OPTION_SCANQRCODE = 2;

    private final static int POSITION_DELAY = 128;
    private final static int MAX_PATHS = 10;

    private final static int REQUEST_READ = 1;
    private final static int REQUEST_SAVE = 2;
    private final static int REQUEST_OPEN = 3;

    private final static int LARGE = 18; // alt : 12, 18, etc.

    private final static int NORMAL = 1; // alt : 2

    private static final int QRSCANNER_ACTIVITY_REQUEST_CODE = 0;
    private static final int SETTINGS_ACTIVITY_REQUEST_CODE = 1;
    private static final int TTS_ACTIVITY_REQUEST_CODE = 2;

    private Pattern languagePattern = Pattern.compile("^\\[\\[(es|it|fr|en|pt|de)\\]\\]");

    private TextToSpeech myTTS;
    final Locale ptLocale = new Locale("pt", "PT");
    final Locale esLocale = new Locale("es");

    private String memtext;
    private File file;
    private String font;
    private String path;
    private String language;
    private Uri content;
    private Uri readUri;
    private EditText textView;
    private ScrollView scrollView;

    private Map<String, Integer> pathMap;
    private List<String> removeList;

    private boolean read = false;
    private boolean ttsReady = false;

    private boolean isApp = false;

    private int size = LARGE;
    private int type = NORMAL;
    private int speechRate = 80;
    private boolean mode;

    // AtomicBoolean pour lancer et stopper la Thread
    private final AtomicBoolean isThreadRunnning = new AtomicBoolean();
    //AtomicBoolean pour mettre en pause et relancer la Thread
    private final AtomicBoolean isThreadPausing = new AtomicBoolean();

    /**
     * onCreate construit la vue associée à l'activité principale
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);

        size = Math.max(5, preferences.getInt(PREF_SIZE, LARGE));
        speechRate = Math.max(preferences.getInt(PREF_SPEECH_RATE, 80), 5);
        font = PREF_FONT;
        memtext = preferences.getString(PREF_MEMTEXT, getResources().getString(R.string.teaser));
        language = preferences.getString(PREF_LANGUAGE, "fr");
        mode = preferences.getBoolean(PREF_MODE, false);

        Set<String> pathSet = preferences.getStringSet(PREF_PATHS, null);
        pathMap = new HashMap<>();

        if (pathSet != null)
            for (String path : pathSet)
                pathMap.put(path, preferences.getInt(path, 0));

        removeList = new ArrayList<>();

        // Texte en mode présentation
        setContentView(R.layout.wrap);
        textView = findViewById(R.id.text);
        scrollView = findViewById(R.id.vscroll);
        textView.setRawInputType(InputType.TYPE_NULL);
        textView.setTextIsSelectable(true);

        // adjust font size et type
        setSizeAndTypeface(font, size, type, mode);

        Intent intent = getIntent();
        Uri uri = intent.getData();

        switch (Objects.requireNonNull(intent.getAction())) {
            case Intent.ACTION_EDIT:
            case Intent.ACTION_VIEW:
                if ((savedInstanceState == null) && (uri != null))
                    readFile(uri);

                Objects.requireNonNull(getActionBar()).setDisplayHomeAsUpEnabled(true);

                isApp = true;
                break;
            case Intent.ACTION_SEND:
                if (savedInstanceState == null) {
                    // Get text
                    String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                    if (text != null) {
                        defaultFile(text);
                    }

                    // Get uri
                    uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                    if (uri != null)
                        readFile(uri);
                }
                isApp = true;
                break;
            case Intent.ACTION_MAIN:
                if (savedInstanceState == null)
                    defaultFile(memtext);
                else {
                    path = savedInstanceState.getString(PATH);
                    content = savedInstanceState.getParcelable(CONTENT);
                    textView.onRestoreInstanceState(savedInstanceState.getParcelable(TEXTVIEW));
                    invalidateOptionsMenu();
                    file = new File(path);
                }

                isApp = true;
        }

        // démarrage de la synthèse vocale
        try {
            Intent checkIntent = new Intent();
            checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkIntent, TTS_ACTIVITY_REQUEST_CODE);
        } catch (Exception ignored) {
        }

        // positionner les listeners
        setListeners();
    }

    private final UtteranceProgressListener mProgressListener = new UtteranceProgressListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onBeginSynthesis(String utteranceId, int sampleRateInHz, int audioFormat, int channelCount) {
            read = true;
            invalidateOptionsMenu();
        }

        @Override
        public void onStop(String utteranceId, boolean interrupted) {
            read = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(memtext);
                }
            });
            invalidateOptionsMenu();
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onStart(String utteranceId) {
            read = true;
            invalidateOptionsMenu();
        }

        @Override
        public void onDone(String utteranceId) {
            // Update menu
            read = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(memtext);
                }
            });
            invalidateOptionsMenu();
        }

        @Override
        public void onError(String utteranceId) {
            // Update menu
            read = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(memtext);
                }
            });
            invalidateOptionsMenu();
        }

        @Override
        public void onRangeStart(String utteranceId, final int start, final int end, int frame) {
            // onRangeStart (and all UtteranceProgressListener callbacks) do not run on main thread
            // ... so we explicitly manipulate views on the main thread:
            int ideb = textView.getSelectionStart();
            int ifin = textView.getSelectionEnd();

            if (ideb == ifin) {
                // if some text is selected, do no highlight anything
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Spannable textWithHighlights = new SpannableString(memtext);
                        textWithHighlights.setSpan(new BackgroundColorSpan(Color.YELLOW), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        textView.setText(textWithHighlights);
                    }
                });
            }
        }
    };

    // setListeners
    private void setListeners() {
        if (textView != null) {
            // onFocusChange
            textView.setOnFocusChangeListener((v, hasFocus) -> {
                // Hide keyboard
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(INPUT_METHOD_SERVICE);
                if (!hasFocus)
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            });

            // onLongClick
            textView.setOnLongClickListener(v -> {
                // Get scroll position
                int y = scrollView.getScrollY();
                // Get height
                int height = scrollView.getHeight();
                // Get width
                int width = scrollView.getWidth();

                // Get offset
                int line = textView.getLayout()
                        .getLineForVertical(y + height / 2);
                int offset = textView.getLayout()
                        .getOffsetForHorizontal(line, width / 2);
                // Set cursor
                textView.setSelection(offset);

                textView.setRawInputType(InputType.TYPE_NULL);

                // Change typeface temporarily as workaround for yet
                // another obscure feature of some versions of android
                textView.setTypeface(Typeface.MONOSPACE, Typeface.NORMAL);
                setSizeAndTypeface(font, size, type, mode);

                return false;
            });

            // remove popup menu if not editable
            textView.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
                @Override
                public boolean onCreateActionMode(ActionMode actionMode, Menu menu) { return true; }
                @Override
                public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                    menu.clear();
                    return false;
                }
                @Override
                public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) { return false; }
                @Override
                public void onDestroyActionMode(ActionMode actionMode) { }
            });

            // onGlobalLayout
            textView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            });

            /*textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textView.setSelection(0);
                }
            });*/
        }

        if (scrollView != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                // onScrollChange
                scrollView.setOnScrollChangeListener((v, x, y, oldX, oldY) -> {
                });

            else
                // onScrollChange
                scrollView.getViewTreeObserver()
                        .addOnScrollChangedListener(() -> {
                        });
        }

        // démarrage de la synthèse vocale
        if (myTTS != null) {
            myTTS.setOnUtteranceProgressListener(mProgressListener);
        }
    }

    // onRestoreInstanceState
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        path = savedInstanceState.getString(PATH);
        content = savedInstanceState.getParcelable(CONTENT);
        textView.onRestoreInstanceState(savedInstanceState.getParcelable(TEXTVIEW));
        invalidateOptionsMenu();
        file = new File(path);

        super.onRestoreInstanceState(savedInstanceState);
    }

    // onDestroy
    protected void onDestroy() {
        // Tuer la Thread
        isThreadRunnning.set(false);

        // Save file
        saveFile();

        // stop TTS
        if (myTTS != null) {
            ttsReady = false;
            myTTS.stop();
            myTTS.shutdown();
        }

        // remettre l'écran dans sa position initiale
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        super.onDestroy();
    }

    // onResume
    protected void onResume() {
        // Relancer la Thread
        isThreadPausing.set(false);
        super.onResume();

        //defaultFile(getResources().getString(R.string.teaser));
    }

    // onPause
    @Override
    public void onPause() {
        isThreadPausing.set(true);

        // Save current path
        savePath(path);

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(PREF_SIZE, size);
        editor.putString(PREF_MEMTEXT, memtext);

        // Add the set of recent files
        editor.putStringSet(PREF_PATHS, pathMap.keySet());

        // Add a position for each file
        try {
            for (String path : pathMap.keySet())
                editor.putInt(path, pathMap.get(path));
        } catch (NullPointerException ignore) {
        }

        // Remove the old ones
        for (String path : removeList)
            editor.remove(path);

        editor.apply();

        // Save file
        saveFile();

        // stop TTS
        if (myTTS != null) {
            myTTS.stop();
            //myTTS.shutdown();
        }

        // remettre l'écran dans sa position initiale
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        super.onPause();
    }

    // onSaveInstanceState
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(CONTENT, content);
        outState.putParcelable(TEXTVIEW, textView.onSaveInstanceState());
        outState.putString(PATH, path);

        super.onSaveInstanceState(outState);
    }

    // onCreateOptionsMenu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    // onPrepareOptionsMenu
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.scanqrcode).setVisible(!read);
        menu.findItem(R.id.speaker).setVisible((!read) && ttsReady);
        menu.findItem(R.id.stop).setVisible(read);

        // Get a list of recent files
        List<Long> list = new ArrayList<>();
        Map<Long, String> map = new HashMap<>();

        // Get the last modified dates
        for (String path : pathMap.keySet()) {
            if (path != null) {
                File file = new File(path);
                long last = file.lastModified();
                list.add(last);
                map.put(last, path);
            }
        }

        // Sort in reverse order
        Collections.sort(list);
        Collections.reverse(list);

        // Get the submenu
        MenuItem item = menu.findItem(R.id.openRecent);
        SubMenu sub = item.getSubMenu();
        sub.clear();

        // Add the recent files
        for (long date : list) {
            String path = map.get(date);

            // Remove path prefix
            assert path != null;
            String name =
                    path.replaceFirst(Environment
                            .getExternalStorageDirectory()
                            .getPath() + File.separator, "");
            sub.add(name);
        }

        // Add clear list item
        sub.add(Menu.NONE, R.id.clearList, Menu.NONE, R.string.clearList);

        return true;
    }

    // onOptionsItemSelected
    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.scanqrcode:
                freshFile(OPTION_SCANQRCODE);
                break;
            case R.id.speaker:
                textToSpeech();
                break;
            case R.id.stop:
                stopTextToSpeech();
                break;
            case R.id.open:
                openFile();
                break;
            case R.id.save:
                saveAs();
                break;
            case R.id.clearList:
                clearList();
                break;
            case R.id.settings:
                settings();
                break;
            case R.id.lgefr:
                setTTSlanguage("fr");
                break;
            case R.id.lgede:
                setTTSlanguage("de");
                break;
            case R.id.lgeen:
                setTTSlanguage("en");
                break;
            case R.id.lgeit:
                setTTSlanguage("it");
                break;
            case R.id.lgees:
                setTTSlanguage("es");
                break;
            case R.id.lgept:
                setTTSlanguage("pt");
                break;
            default:
                openRecent(item);
                break;
        }

        return true;
    }

    // onBackPressed
    @Override
    public void onBackPressed() {
        if (myTTS != null) {
            //myTTS.stop();
            myTTS.shutdown();
        }
        finish();
    }

    public void setTTSlanguage(String lge) {
        language = lge;

        // change language if needed
        switch(language) {
            case "en": myTTS.setLanguage(Locale.ENGLISH); break;
            case "de": myTTS.setLanguage(Locale.GERMAN); break;
            case "it": myTTS.setLanguage(Locale.ITALIAN); break;
            case "es": myTTS.setLanguage(esLocale); break;
            case "pt": myTTS.setLanguage(ptLocale); break;
            default: myTTS.setLanguage(Locale.FRANCE); break;
        }

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_LANGUAGE, language);
        editor.apply();

        setDocumentTitle();
    }

    // textToSpeech
    private void textToSpeech() {
        String portion = "";

        int ideb = textView.getSelectionStart();
        int ifin = textView.getSelectionEnd();

        if (ideb == ifin) {
            // ne conserver que la portion de texte visible
            Layout layout = textView.getLayout();
            if (layout != null) {
                int height = scrollView.getHeight();
                int scrollY = scrollView.getScrollY();
                int firstVisibleLineNumber = layout.getLineForVertical(scrollY);
                int lastVisibleLineNumber = layout.getLineForVertical(scrollY + height);
                portion = textView.getText().toString().substring(layout.getLineStart(firstVisibleLineNumber),
                        layout.getLineEnd(lastVisibleLineNumber));
            }
        } else {
            portion = textView.getText().toString().substring(ideb, ifin);
        }

        // tts
        if ((myTTS != null) && (portion.length() > 0)) {
            // Update menu
            read = true;
            invalidateOptionsMenu();

            myTTS.speak(portion, TextToSpeech.QUEUE_FLUSH, null, "ArkalineQRokeli");
        }
    }

    // textToSpeech
    private void stopTextToSpeech() {
        if (myTTS != null) {
            myTTS.stop();
        }

        // Update menu
        read = false;
        invalidateOptionsMenu();
    }

    private void setDocumentTitle() {
        String title = getResources().getString(R.string.appName)+" - "+language;
        setTitle(title);
    }

    // freshFile
    private void freshFile(int option) {
        switch (option) {
            case OPTION_SCANQRCODE:
                scanQRCode();
                break;
        }
    }

    // scanQRCode
    private void scanQRCode() {
        Intent scanActivity = new Intent(MainActivity.this, ScanQRCodeActivity.class);
        startActivityForResult(scanActivity, QRSCANNER_ACTIVITY_REQUEST_CODE);

        // simulation de scan de qr code
        /*Random r = new Random();
        int lqrcodes[] = {R.drawable.qr1, R.drawable.qr2, R.drawable.qr3};
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), lqrcodes[r.nextInt(lqrcodes.length)]);
        try {
            String s = new QrDecoder(getApplicationContext()).decode(bitmap.getWidth(), bitmap.getHeight(), nv21(bitmap));

            if (!isDefaultFile())
                file = getDefaultFile();
            Uri uri = Uri.fromFile(file);
            path = uri.getPath();

            // get String data from Intent
            Log.d("SCANCODE", s);

            // set text view with string
            formatText(s);
            setDocumentTitle();

            textView.setRawInputType(InputType.TYPE_NULL);

            // set focusable
            textView.setFocusable(false);

            // Update menu
            invalidateOptionsMenu();
        } catch (Exception e) {
        }*/
    }

    // settings
    private void settings() {
        // conserver les infos de préférence avant l'activation de l'activité de settings
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PREF_SIZE, size);
        editor.putString(PREF_MEMTEXT, memtext);
        editor.apply();

        // activation de l'activité de settings
        Intent settingsActivity = new Intent(MainActivity.this, SettingsActivity.class);
        startActivityForResult(settingsActivity, SETTINGS_ACTIVITY_REQUEST_CODE);
    }

    // getNewFile
    private File getNewFile() {
        File documents = new File(Environment.getExternalStorageDirectory(), DOCUMENTS);
        return new File(documents, DEFAULT_FILE);
    }

    // isNewFile
    private boolean isDefaultFile()
    {
        return (file.toString().endsWith(DEFAULT_FILE));
    }

    // getDefaultFile
    private File getDefaultFile()
    {
        File documents = new File(Environment.getExternalStorageDirectory(), DOCUMENTS);
        return new File(documents, DEFAULT_FILE);
    }

    // defaultFile
    private void defaultFile(String text)
    {
        file = getDefaultFile();

        Uri uri = Uri.fromFile(file);
        path = uri.getPath();

        setDocumentTitle();
        if ((text != null) && (text.length() > 0)) {
            textView.setText(text);
        } else {
            textView.setText(getResources().getString(R.string.teaser));
        }

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        memtext = textView.getText().toString();
        editor.putString(PREF_MEMTEXT, memtext);
        editor.apply();

    }

    // This method is called when a qr code is scanned or when returning from settings
    @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == QRSCANNER_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK
                if (!isDefaultFile())
                    file = getDefaultFile();
                Uri uri = Uri.fromFile(file);
                path = uri.getPath();

                // get String data from Intent
                String decText = data.getStringExtra("text").trim();

                // enable to code the language into the QR Code
                Matcher matcher = languagePattern.matcher(decText);
                if (matcher.find()) {
                    language = decText.substring(matcher.start()+2, matcher.end()-2);
                    setTTSlanguage(language);

                    memtext = (decText.substring(0,matcher.start())+decText.substring(matcher.end())).trim();
                } else {
                    memtext = decText;
                }

                SharedPreferences preferences =
                        PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(PREF_MEMTEXT, memtext);
                editor.apply();

                textView.setText(memtext);

                // set text view with string
                setDocumentTitle();

                textView.setRawInputType(InputType.TYPE_NULL);

                // set focusable
                //textView.setFocusable(false);

                // Update menu
                invalidateOptionsMenu();
            }
        }

        if (requestCode == SETTINGS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK
                SharedPreferences preferences =
                        PreferenceManager.getDefaultSharedPreferences(this);

                // get parameters from the user's profile
                size = Math.max(5, preferences.getInt(PREF_SIZE, LARGE));
                speechRate = Math.max(preferences.getInt(PREF_SPEECH_RATE, 80), 5);
                if (myTTS != null) {
                    myTTS.setSpeechRate(speechRate * 0.01f);
                }
                mode = preferences.getBoolean(PREF_MODE, false);

                // adjust font size et type
                setSizeAndTypeface(font, size, type, mode);

                // afficher le dernier texte présenté
                textView.setText(memtext);

                // setDocumentTitle
                setDocumentTitle();

                // Update menu
                invalidateOptionsMenu();
            }
        }

        if (requestCode == TTS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // success, create the TTS instance
                myTTS = new TextToSpeech(this, this);
                myTTS.setOnUtteranceProgressListener(mProgressListener);
            } else {
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    // alertDialog
    private void alertDialog(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.appName);
        builder.setMessage(message);

        // Add the buttons
        builder.setNeutralButton(R.string.ok, null);

        // Create the AlertDialog
        builder.show();
    }

    // savePath
    private void savePath(String path)
    {
        // Save the current position
        pathMap.put(path, scrollView.getScrollY());

        // Get a list of files
        List<Long> list = new ArrayList<>();
        Map<Long, String> map = new HashMap<>();
        for (String name : pathMap.keySet())
        {
            if (name != null) {
                File file = new File(name);
                list.add(file.lastModified());
                map.put(file.lastModified(), name);
            }
        }

        // Sort in reverse order
        Collections.sort(list);
        Collections.reverse(list);

        int count = 0;
        for (long date : list)
        {
            String name = map.get(date);

            // Remove old files
            if (count >= MAX_PATHS)
            {
                pathMap.remove(name);
                removeList.add(name);
            }

            count++;
        }
    }

    // openRecent
    private void openRecent(MenuItem item)
    {
        String name = item.getTitle().toString();
        File file = new File(name);

        // Check absolute file
        if (!file.isAbsolute())
            file = new File(Environment.getExternalStorageDirectory(),
                            File.separator + name);
        // Check it exists
        if (file.exists())
        {
            final Uri uri = Uri.fromFile(file);
            readFile(uri);
        }
    }

    // saveAs
    private void saveAs()
    {
        // Remove path prefix
        String name =
            path.replaceFirst(Environment
                              .getExternalStorageDirectory()
                              .getPath() + File.separator, "");

        // Open dialog
        saveAsDialog(name, (dialog, id) ->
        {
            if (id == DialogInterface.BUTTON_POSITIVE) {
                EditText text = ((Dialog) dialog).findViewById(R.id.path_text);
                String string = text.getText().toString();

                // Ignore empty string
                if (string.isEmpty())
                    return;

                file = new File(string);

                // Check absolute file
                if (!file.isAbsolute())
                    file = new
                        File(Environment.getExternalStorageDirectory(), string);

                // Set interface title
                setDocumentTitle();

                path = file.getPath();
                saveFile(file);
            }
        });
    }

    // saveAsDialog
    private void saveAsDialog(String path,
                              DialogInterface.OnClickListener listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.save);
        builder.setMessage(R.string.choose);

        // Add the buttons
        builder.setPositiveButton(R.string.save, listener);
        builder.setNegativeButton(R.string.cancel, listener);

        // Create edit text
        Context context = builder.getContext();
        EditText text = new EditText(context);
        text.setId(R.id.path_text);
        text.setText(path);

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.setView(text, 40, 0, 40, 0);
        dialog.show();
    }

    // clearList
    private void clearList()
    {
        for (String path : pathMap.keySet())
            removeList.add(path);

        pathMap.clear();
    }

    // setSizeAndTypeface
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setSizeAndTypeface(String ft, int size, int type, boolean mode)
    {
        // reference : https://developer.android.com/reference/android/widget/TextView
        // Set font
        Typeface lecfont = Typeface.createFromAsset(getAssets(), ft);
        textView.setTypeface(lecfont, type);

        // Set size
        textView.setTextSize(size);

        // Increase line height : 1.5
        textView.setLineSpacing(0,1.5f);

        if (mode) {
            setTheme(R.style.Theme_QRokeliSombre);
            textView.setBackgroundResource(R.color.dark);
            textView.setTextColor(ContextCompat.getColor(this, R.color.light));
        } else {
            setTheme(R.style.Theme_QRokeliClair);
            textView.setBackgroundResource(R.color.light);
            textView.setTextColor(ContextCompat.getColor(this, R.color.dark));
        }
    }

    /* private byte[] nv21(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int [] argb = new int[w * h];
        bitmap.getPixels(argb, 0, w, 0, 0, w, h);
        byte[] yuv = new byte[w*h + (h+1)*(w+1)/2];
        encodeYUV420SP(yuv, argb, w, h);
        bitmap.recycle();
        return yuv;
    }

    private void encodeYUV420SP(byte[] yuv420sp, int[] argb, int width, int height) {
        int frameSize = width * height;
        int yIndex = 0;
        int uvIndex = frameSize;
        int R, G, B, Y, U, V;
        int index = 0;
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                R = (argb[index] & 0xff0000) >> 16;
                G = (argb[index] & 0xff00) >> 8;
                B = (argb[index] & 0xff);

                Y = ( (  66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
                U = ( ( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
                V = ( ( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;

                yuv420sp[yIndex++] = (byte) ((Y < 0) ? 0 : Math.min(Y, 255));
                if (j % 2 == 0 && index % 2 == 0) {
                    yuv420sp[uvIndex++] = (byte)((V<0) ? 0 : Math.min(V, 255));
                    yuv420sp[uvIndex++] = (byte)((U<0) ? 0 : Math.min(U, 255));
                }
                index++;
            }
        }
    }*/

    // openFile
    private void openFile()
    {
        /*Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.qr1);
        String s = new QrDecoder(getApplicationContext()).decode(bitmap.getWidth(), bitmap.getHeight(), nv21(bitmap));

        return;*/

        getFile();
    }

    // getFile
    private void getFile()
    {
        // Check permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]
                    {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                     Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_OPEN);
                return;
            }
        }

        // Open parent folder
        File dir = file.getParentFile();
        getFile(dir);
    }

    // getFile
    private void getFile(File dir)
    {
        // Get list of files
        List<File> list = getList(dir);

        // Pop up dialog
        String title = FOLDER + dir.getPath();
        openDialog(title, list, (dialog, which) ->
            {
                File selection = list.get(which);
                if (selection.isDirectory())
                    getFile(selection);

                else
                    readFile(Uri.fromFile(selection));
            });
    }

    // getList
    private List<File> getList(File dir)
    {
        List<File> list;
        File[] files = dir.listFiles();
        // Check files
        if (files == null)
        {
            // Create a list with just the parent folder and the
            // external storage folder
            list = new ArrayList<>();

            if (dir.getParentFile() == null)
                list.add(dir);

            else
                list.add(dir.getParentFile());

            list.add(Environment.getExternalStorageDirectory());
            return list;
        }

        // Sort the files
        Arrays.sort(files);
        // Create a list
        list = new ArrayList<>(Arrays.asList(files));
        // Remove hidden files
        Iterator<File> iterator = list.iterator();
        while (iterator.hasNext())
        {
            File item = iterator.next();
            if (item.getName().startsWith("."))
                iterator.remove();
        }

        // Add parent folder
        if (dir.getParentFile() == null)
            list.add(0, dir);

        else
            list.add(0, dir.getParentFile());

        return list;
    }

    // openDialog
    private void openDialog(String title, List<File> list,
                            DialogInterface.OnClickListener listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);

        // Add the adapter
        FileAdapter adapter = new FileAdapter(builder.getContext(), list);
        builder.setAdapter(adapter, listener);

        // Add the button
        builder.setNegativeButton(R.string.cancel, null);

        // Create the Dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // onRequestPermissionsResult
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
        case REQUEST_SAVE:
            for (int i = 0; i < grantResults.length; i++)
                if (permissions[i].equals(Manifest.permission
                                          .WRITE_EXTERNAL_STORAGE) &&
                    grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    // Granted, save file
                    saveFile();
            break;

        case REQUEST_READ:
            for (int i = 0; i < grantResults.length; i++)
                if (permissions[i].equals(Manifest.permission
                                          .READ_EXTERNAL_STORAGE) &&
                    grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    // Granted, read file
                    readFile(readUri);
            break;

        case REQUEST_OPEN:
            for (int i = 0; i < grantResults.length; i++)
                if (permissions[i].equals(Manifest.permission
                                          .READ_EXTERNAL_STORAGE) &&
                    grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    // Granted, open file
                    getFile();
            break;
        }
    }

    // readFile
    private void readFile(Uri uri) {
        if (uri == null)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]
                    {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                     Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ);
                readUri = uri;
                return;
            }
        }

        content = null;

        // Attempt to resolve content uri
        if (CONTENT.equalsIgnoreCase(uri.getScheme()))
            uri = resolveContent(uri);

        // Read into default file if unresolved
        if (CONTENT.equalsIgnoreCase(uri.getScheme()))
        {
            content = uri;
            file = getDefaultFile();
            Uri defaultUri = Uri.fromFile(file);
            path = defaultUri.getPath();
        }
        // Read file
        else
        {
            path = uri.getPath();
            file = new File(path);
        }
        setDocumentTitle();

        textView.setText(R.string.loading);

        ReadTask read = new ReadTask(this);
        read.execute(uri);

        savePath(path);
        invalidateOptionsMenu();
    }

    // resolveContent
    private Uri resolveContent(Uri uri)
    {
        String path = FileUtils.getPath(this, uri);

        if (path != null)
        {
            File file = new File(path);
            if (file.canRead())
                uri = Uri.fromFile(file);
        }

        return uri;
    }

    // saveFile
    private void saveFile()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]
                    {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                     Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_SAVE);
                return;
            }
        }

        if (content == null)
            saveFile(file);
        else
            saveFile(content);
    }

    // saveFile
    private void saveFile(File file)
    {
        CharSequence text = textView.getText();
        write(text, file);
    }

    // saveFile
    private void saveFile(Uri uri)
    {
        CharSequence text = textView.getText();
        try (OutputStream outputStream =
             getContentResolver().openOutputStream(uri))
        {
            write(text, outputStream);
        }

        catch (Exception e)
        {
            alertDialog(e.getMessage());
            e.printStackTrace();
        }
    }

    // write
    private void write(CharSequence text, File file)
    {
        if (file == null)
            return;
        file.getParentFile().mkdirs();
        try (FileWriter fileWriter = new FileWriter(file))
        {
            fileWriter.append(text);
        }

        catch (Exception e)
        {
            //alertDialog(e.getMessage());
            //e.printStackTrace();
            return;
        }

        invalidateOptionsMenu();
        savePath(file.getPath());
    }

    // write
    private void write(CharSequence text, OutputStream os)
    {
        try (OutputStreamWriter writer = new OutputStreamWriter(os))
        {
            writer.append(text);
        }

        catch (Exception e)
        {
            alertDialog(e.getMessage());
            e.printStackTrace();
            return;
        }

        invalidateOptionsMenu();
    }

    // loadText
    private void loadText(CharSequence text)
    {
        if (textView == null) return;
        memtext = text.toString();
        textView.setText(text);

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_MEMTEXT, memtext);
        editor.apply();

        // Check for saved position
        if (pathMap.containsKey(path))
            textView.postDelayed(() ->
                                 scrollView.smoothScrollTo
                                 (0, pathMap.get(path)),
                                 POSITION_DELAY);
        else
            textView.postDelayed(() ->
                                 scrollView.smoothScrollTo(0, 0),
                                 POSITION_DELAY);

        // Dismiss keyboard
        textView.clearFocus();

        // Update menu
        invalidateOptionsMenu();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            setTTSlanguage(language);
            myTTS.setSpeechRate(speechRate*0.01f);
            ttsReady = true;

            // Update menu
            invalidateOptionsMenu();
        }
    }

    // ReadTask
    private static class ReadTask extends AsyncTask<Uri, Void, CharSequence>
    {
        private final WeakReference<MainActivity> editorWeakReference;

        public ReadTask(MainActivity editor)
        {
            editorWeakReference = new WeakReference<>(editor);
        }

        // doInBackground
        @Override
        protected CharSequence doInBackground(Uri... uris)
        {
            StringBuilder stringBuilder = new StringBuilder();
            final MainActivity editor = editorWeakReference.get();
            if (editor == null)
                return stringBuilder;

            try (InputStream inputStream = editor.getContentResolver()
                 .openInputStream(uris[0]);
                 BufferedReader reader = new BufferedReader
                 (new InputStreamReader(inputStream)))
            {
                String line;
                while ((line = reader.readLine()) != null)
                {
                    stringBuilder.append(line);
                    stringBuilder.append(System.getProperty("line.separator"));
                }
            }

            catch (Exception e)
            {
                editor.runOnUiThread(() ->
                                     editor.alertDialog(e.getMessage()));
                e.printStackTrace();
            }

            return stringBuilder;
        }

        // onPostExecute
        @Override
        protected void onPostExecute(CharSequence result)
        {
            final MainActivity editor = editorWeakReference.get();
            if (editor == null)
                return;

            editor.loadText(result);
        }
    }
}
