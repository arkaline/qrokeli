# ![Logo](src/main/res/drawable-hdpi/ic_launcher.png) QRokèli

Scanne un QR code et lit le texte correspondant

QRokèli est un application qui permet de faire lire un texte scanné par l'intermédiaire d'un QR Code.

Cette application n'utilise ni ne collecte aucune information concernant son utilisateur.
